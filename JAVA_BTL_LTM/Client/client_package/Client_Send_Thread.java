package client_package;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import javax.sound.sampled.TargetDataLine;

public class Client_Send_Thread extends Thread {

	// khởi tạo lại các biến, đối tượng để thực thi
	public TargetDataLine audioIn_THR =null;
	public DatagramSocket socket;
	byte [] byteBuffer = new byte[1024];
	public InetAddress ipAddr;
	public int port;
	public void run() {
		while(Client_Run.sending==true) {	// dùng điều kiện này để làm trạng thái bắt sự kiện khi bấm nút start
			try {
				audioIn_THR.read(byteBuffer,0, byteBuffer.length); // đọc dữ liệu vào bytebuffer
				DatagramPacket datPacket = new DatagramPacket(byteBuffer, byteBuffer.length, ipAddr, port);	//chuyển dữ liệu trong buffer bằng phương thức UDP
				System.out.println("client_sending");		//thông báo màn hình console để biết đang gửi dữ liệu
				socket.send(datPacket);						// gửi.
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		audioIn_THR.close();			// đóng cái targetdataline lại nếy trạng thái connecting = false
		audioIn_THR.drain();			// giải phóng giữ liệu trong hàng đợi cho đến khi mảng bytebuffer rỗng
		
		System.out.println("Client Stoped Sending Data");
	}
}
