package client_package;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import javax.sound.sampled.SourceDataLine;




public class Client_Rec_Thead extends Thread {
	public DatagramSocket socket;
	public SourceDataLine Speaker_Thr;
	byte buffer[] = new byte[1024];
	public void run() {
		DatagramPacket voiceRecv = new DatagramPacket(buffer, buffer.length);
		while(Client_Run.receiving == true) {
			try {				
				socket.receive(voiceRecv);
				buffer = voiceRecv.getData();
				Speaker_Thr.write(buffer, 0, buffer.length);
				System.out.println("client receive");	
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		Speaker_Thr.close();
		Speaker_Thr.drain();
		System.err.println("client listen stopped");
		
	}
}
