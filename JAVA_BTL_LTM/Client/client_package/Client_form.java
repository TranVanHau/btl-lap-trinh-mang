package client_package;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.awt.event.ActionEvent;

public class Client_form extends JFrame {

	private JPanel contentPane;
	private JTextField CL_S_port_Txt;			

	public String IPAdd = "127.0.0.1";		// địa chỉ IP
	
	
	public static AudioFormat getAudioFormat() {	
		
		// sử dụng class AudioFormat để mã hóa âm thanh với các cài đặt tham số
		// sampleRate = 24000.0F : Số Rate nghe lấy trên 1s (mỗi lầ nghe gọi là sample)
		// sampleSiziInBits = 16 :số bit mỗi lần nghe lấy 
		// channels =2 : nghe bằng 2 kênh là trái và phải
		// signed = true : dữ liệu được đánh dấu hay k đc đánh giấu
		//bigEndian =false : cơ chế sắp xếp dữ liệu khi lưu là BigEndian (dạng ngược)
		AudioFormat a = new AudioFormat(24000.0F,16,2,true,false);
		return a;
	}
		
	
	
	
		// nói
		public void CL_init_Send(int port) {	
				try {
					AudioFormat format = getAudioFormat();	
					// lấy các thông tin các dữ liệu được hỗ trợ
					DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, format);
					
					// kiểm tra dư liệu lấy từ thiết bị có đc hỗ trợ hay không
					if(!AudioSystem.isLineSupported(dataLineInfo)) {
						System.out.println("not support");
						System.exit(0);
					}
					
					// lấy dữ liệu đọc được lưu vào mic
					// lấy giữ liệu phù hợp với chỉ định cảu datalineInfo (dữ liệu được hỗ trợ).
					TargetDataLine mic =(TargetDataLine) AudioSystem.getLine(dataLineInfo);

					mic.open(format);										// 
					mic.start();											//Bắt Đầu thu âm thanh theo thông số đã cấu hình
					Client_Send_Thread VT =  new Client_Send_Thread();		// chạy thread để gửi audio
					InetAddress inet = InetAddress.getByName(IPAdd);	// khởi tạo lại các tham số so với Thread
					VT.audioIn_THR = mic;
					VT.socket = new DatagramSocket();
					VT.ipAddr=inet;
					VT.port = port;
					Client_Run.sending=true;						//chuyển trạng thái connectting thành true để gửi đc voice (ở thread)
					VT.start();										// chạy Thread Gửi
					
				}catch(Exception e) {
					e.printStackTrace();
				}
				
		}
		
		// nghe
		
		public void CL_init_Rec(int port) {	
			try {
				AudioFormat format = getAudioFormat();	
				// lấy các thông tin các dữ liệu được hỗ trợ
				DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
				// kiểm tra dư liệu lấy từ thiết bị có đc hỗ trợ hay không
				if(!AudioSystem.isLineSupported(dataLineInfo)) {
					System.out.println("not support");
					System.exit(0);
				}
				
				// lấy giữ liệu phù hợp với chỉ định cảu datalineInfo (dữ liệu được hỗ trợ).
				SourceDataLine Speaker = (SourceDataLine)AudioSystem.getLine(dataLineInfo);
				Speaker.open(format);	
				Speaker.start();	
				Client_Rec_Thead RC =  new Client_Rec_Thead();
				RC.socket = new DatagramSocket(port);
				RC.Speaker_Thr = Speaker;
				Client_Run.receiving=true;		
				RC.start();

			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client_form frame = new Client_form();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	

	/**
	 * Create the frame.
	 */
	public Client_form() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Client");
		lblNewLabel.setFont(new Font("Segoe UI", Font.BOLD, 25));
		lblNewLabel.setBounds(10, 10, 113, 29);
		contentPane.add(lblNewLabel);
		
		
		CL_start_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(CL_S_port_Txt.getText().equals("")||CL_R_Port_txt.getText().equals("")) {
					JFrame fr= new JFrame();
					JOptionPane.showMessageDialog(fr,"chưa nhập port");
				}
				else {
					Integer SendportVal = Integer.parseInt(CL_S_port_Txt.getText());
					Integer ReceiveportVal = Integer.parseInt(CL_R_Port_txt.getText());
					if(SendportVal>0&&SendportVal<10000&&ReceiveportVal>0&&ReceiveportVal<10000) {
						
						CL_init_Send(SendportVal);
						CL_init_Rec(ReceiveportVal);
						CL_start_btn.setEnabled(false);
						CL_stop_btn.setEnabled(true);
					}
					else {
						JFrame fr= new JFrame();
						JOptionPane.showMessageDialog(fr,"port không đúng");
					}
				}
			}
		});
		CL_start_btn.setFont(new Font("Segoe UI", Font.BOLD, 15));
		CL_start_btn.setBounds(48, 273, 96, 46);
		contentPane.add(CL_start_btn);
		
		
		CL_stop_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Arrays.asList(t1, t2).parallelStream().forEach(Thread::interrupt);
				Client_Run.receiving=false;
				CL_start_btn.setEnabled(true);
				CL_stop_btn.setEnabled(false);
				
				
			}
		});
		CL_stop_btn.setFont(new Font("Segoe UI", Font.BOLD, 15));
		CL_stop_btn.setBounds(260, 273, 96, 46);
		contentPane.add(CL_stop_btn);
		
		CL_S_port_Txt = new JTextField();
		CL_S_port_Txt.setText("9999");
		CL_S_port_Txt.setBounds(198, 63, 96, 21);
		contentPane.add(CL_S_port_Txt);
		CL_S_port_Txt.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Send Port : ");
		lblNewLabel_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblNewLabel_1.setBounds(48, 55, 123, 27);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Receive Port : ");
		lblNewLabel_1_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblNewLabel_1_1.setBounds(20, 122, 151, 27);
		contentPane.add(lblNewLabel_1_1);
		
		CL_R_Port_txt = new JTextField();
		CL_R_Port_txt.setText("8888");
		CL_R_Port_txt.setColumns(10);
		CL_R_Port_txt.setBounds(198, 122, 96, 21);
		contentPane.add(CL_R_Port_txt);
	}
	
	
	// button 
	private JButton CL_start_btn = new JButton("Start");
	private JButton CL_stop_btn = new JButton("Stop");
	private JTextField CL_R_Port_txt;
}
