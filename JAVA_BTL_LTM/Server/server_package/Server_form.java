package server_package;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.html.HTMLEditorKit.Parser;


import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class Server_form extends JFrame {

	private JPanel contentPane;
	private JTextField SV_port;
	
	public String IPAdd = "127.0.0.1";		// địa chỉ IP
	
	
	
	public static AudioFormat getAudioFormat() {
		return new AudioFormat(24000.0F, 16, 2, true,false);
	}
	
// nghe
	public void init_Rec(int port) {	
		try {
			AudioFormat format = getAudioFormat();	
			DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);

			if(!AudioSystem.isLineSupported(dataLineInfo)) {
				System.out.println("not support");
				System.exit(0);
			}
			
			SourceDataLine Speaker = (SourceDataLine)AudioSystem.getLine(dataLineInfo);
			Speaker.open(format);	
			Speaker.start();		
			Server_Rec_Thread RC =  new Server_Rec_Thread();			
			RC.socket = new DatagramSocket(port);
			RC.Thread_port=port;
			RC.Speaker_Thr = Speaker;
			Server_Run2.receiving=true;		
			RC.start();

		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	// nói
	
	// nói
		public void init_Send(int port) {	
			try {
				AudioFormat format = getAudioFormat();	
				// lấy các thông tin các dữ liệu được hỗ trợ
				DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, format);
				
				// kiểm tra dư liệu lấy từ thiết bị có đc hỗ trợ hay không
				if(!AudioSystem.isLineSupported(dataLineInfo)) {
					System.out.println("not support");
					System.exit(0);
				}
				
				// lấy dữ liệu đọc được lưu vào mic
				// lấy giữ liệu phù hợp với chỉ định cảu datalineInfo (dữ liệu được hỗ trợ).
				TargetDataLine mic =(TargetDataLine) AudioSystem.getLine(dataLineInfo);

				mic.open(format);	//
				mic.start();		//
				Server_Send_Thread VT =  new Server_Send_Thread();		// cháº¡y thread Ä‘á»ƒ gá»­i audio
				InetAddress inet = InetAddress.getByName(IPAdd);	// khá»Ÿi táº¡o láº¡i cÃ¡c tham sá»‘ so vá»›i Thread
				VT.audioIn_THR = mic;
				VT.socket = new DatagramSocket();
				VT.ipAddr=inet;
				VT.port = port;
				Server_Run2.sending=true;						// chuyá»ƒn tráº¡ng thÃ¡i connectting thÃ nh true Ä‘á»ƒ gá»­i Ä‘c voice (á»Ÿ thread)
				VT.start();
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Server_form frame = new Server_form();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	


	/**
	 * Create the frame.
	 */
	public Server_form() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 402);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		SV_start_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SV_port.getText().equals("")||SV_R_port.getText().equals("")) {
					JFrame fr= new JFrame();
					JOptionPane.showMessageDialog(fr,"chưa nhập port");
				}
				else {
					Integer SendportVal = Integer.parseInt(SV_port.getText());
					Integer ReceiveportVal = Integer.parseInt(SV_R_port.getText());
					if(SendportVal>0&&SendportVal<10000&&ReceiveportVal>0&&ReceiveportVal<10000) {
						init_Send(SendportVal);
						init_Rec(ReceiveportVal);
						SV_start_btn.setEnabled(false);
						SV_stop_btn.setEnabled(true);
					}
					else {
						JFrame fr= new JFrame();
						JOptionPane.showMessageDialog(fr,"port không đúng");
					}
				}

			}
		});
		SV_start_btn.setFont(new Font("Segoe UI", Font.BOLD, 15));
		SV_start_btn.setBounds(43, 262, 96, 46);
		contentPane.add(SV_start_btn);
		
		
		SV_stop_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Server_Run2.receiving = false;
				Server_Run2.sending = false;
				//Arrays.asList(t3, t4).parallelStream().forEach(Thread::interrupt);
				SV_start_btn.setEnabled(true);
				SV_stop_btn.setEnabled(false);
			}
		});
		SV_stop_btn.setFont(new Font("Segoe UI", Font.BOLD, 15));
		SV_stop_btn.setBounds(247, 262, 96, 46);
		contentPane.add(SV_stop_btn);
		
		SV_port = new JTextField();
		SV_port.setText("8888");
		SV_port.setColumns(10);
		SV_port.setBounds(247, 84, 96, 21);
		contentPane.add(SV_port);
		
		JLabel lblNewLabel_1 = new JLabel("Send Port : ");
		lblNewLabel_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblNewLabel_1.setBounds(52, 76, 128, 27);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblServer = new JLabel("Server");
		lblServer.setFont(new Font("Segoe UI", Font.BOLD, 25));
		lblServer.setBounds(26, 20, 113, 29);
		contentPane.add(lblServer);
		
		lblNewLabel = new JLabel("Receive Port : ");
		lblNewLabel.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblNewLabel.setBounds(26, 138, 146, 27);
		contentPane.add(lblNewLabel);
		
		SV_R_port = new JTextField();
		SV_R_port.setText("9999");
		SV_R_port.setColumns(10);
		SV_R_port.setBounds(247, 146, 96, 21);
		contentPane.add(SV_R_port);
	}
	
	private JButton SV_stop_btn = new JButton("Stop");
	private JButton SV_start_btn = new JButton("Start");
	private JLabel lblNewLabel;
	private JTextField SV_R_port;
}
