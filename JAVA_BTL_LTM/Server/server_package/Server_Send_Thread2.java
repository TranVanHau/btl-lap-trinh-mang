package server_package;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.sound.sampled.TargetDataLine;

import client_package.Client_Run;

public class Server_Send_Thread extends Thread {
	// khởi tạo lại các biến, đối tượng để thực thi
		public TargetDataLine audioIn_THR =null;
		public DatagramSocket socket;
		public InetAddress ipAddr;
		byte [] byteBuffer = new byte[1024];
		public int port;
		public void run() {
			while(Server_Run2.sending==true) {	// dùng điều kiện này để làm trạng thái bắt sự kiện khi bấm nút start
				try {
					audioIn_THR.read(byteBuffer,0, byteBuffer.length); // đọc dữ liệu vào bytebuffer
					DatagramPacket datPacket = new DatagramPacket(byteBuffer, byteBuffer.length,ipAddr,port);	//chuyển dữ liệu trong buffer bằng phương thức UDP
					System.err.println("server_sending");		//thông báo màn hình console để biết đang gửi dữ liệu
					socket.send(datPacket);						// gửi.
				}catch(IOException e) {
					e.printStackTrace();
				}
			}
			
			audioIn_THR.close();			// đóng cái targetdataline lại nếy trạng thái connecting = false
			audioIn_THR.drain();			// giải phóng giữ liệu trong hàng đợi cho đến khi mảng bytebuffer rỗng
			System.err.println("Server Stoped Sending Data");
		}
}
