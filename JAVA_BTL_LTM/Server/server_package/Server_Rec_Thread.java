package server_package;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import javax.sound.sampled.SourceDataLine;

public class Server_Rec_Thread extends Thread {
	public DatagramSocket socket;
	public SourceDataLine Speaker_Thr;
	byte buffer[] = new byte[1024];
	public int Thread_port;
	public void run() {
		DatagramPacket voiceRecv = new DatagramPacket(buffer, buffer.length);
		while(Server_Run2.receiving == true) {
			try {
				socket.receive(voiceRecv);		// nhận dữ liệu âm thanh từ socket này
				buffer = voiceRecv.getData();		// trả về bộ đệm lưu data vào trong buffer
				Speaker_Thr.write(buffer, 0, buffer.length);//Giai
				System.out.println("server receive");
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		Speaker_Thr.close();
		Speaker_Thr.drain();
		System.err.println("server Listen stopped");
		
	}
}
